package com.example.demo.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Posts {
	 private Map<String, Post> posts = new HashMap<>();
	 
	 public Posts () {
		Post post1 = new Post();
		post1.setId("1");
		post1.setPost("My first post");
		posts.put(post1.getId(), post1);
		
		Post post2 = new Post();
		post2.setId("2");
		post2.setPost("My seconde post3");
		posts.put(post2.getId(), post2);
	 }
	 
	 public Collection<Post> all(){
		 return posts.values();
	 }

	public Post get(String id) {
		// TODO Auto-generated method stub
		return posts.get(id);
	}

	public void insert(Post post) {
		// TODO Auto-generated method stub
		posts.put(post.getId(),post);
	}

}
