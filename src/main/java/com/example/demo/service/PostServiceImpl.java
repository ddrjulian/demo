package com.example.demo.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.example.demo.model.Post;
import com.example.demo.model.Posts;

@Service
public  class PostServiceImpl implements PostService {

	private Posts _postRepo = new Posts();
	
	@Override
	public Collection<Post> get_posts() {
		// TODO Auto-generated method stub
		return _postRepo.all();
	}
	
	@Override
	public Post get_post(String id) {
		return _postRepo.get(id);
	}

	@Override
	public String create_post(Post post) {
		// TODO Auto-generated method stub
		_postRepo.insert(post);
		return "Post is created successfully";
	}
	


}
