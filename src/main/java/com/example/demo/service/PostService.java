package com.example.demo.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.example.demo.model.Post;

@Service
public interface PostService {
	
	public Collection<Post> get_posts();
	public Post get_post(String id);
	public String create_post(Post post);

}
