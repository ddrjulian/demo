package com.example.demo.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Post;
import com.example.demo.model.Posts;
import com.example.demo.service.PostService;

@RestController
public class PostController {
	
	
	@Autowired
	PostService postService;
   
   @GetMapping(value = "/post")
   public ResponseEntity<Object> getPost() {
	  Collection<Post> allPosts = postService.get_posts();
      return new ResponseEntity<>(allPosts, HttpStatus.OK);
   }
   
   @GetMapping(value = "/post/{id}")
   public ResponseEntity<Object> getPostbyid(@PathVariable("id") String id) {
	   
      return new ResponseEntity<>(postService.get_post(id), HttpStatus.OK);
   }
//   
//   @RequestMapping(value = "/post/{id}", method = RequestMethod.DELETE)
//   public ResponseEntity<Object> delete(@PathVariable("id") String id) { 
//	   postRepo.remove(id);
//      return new ResponseEntity<>("Post is deleted successsfully", HttpStatus.OK);
//   }
//   
//   @RequestMapping(value = "/post/{id}", method = RequestMethod.PUT)
//   public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Post post) { 
//	  postRepo.remove(id);
//      post.setId(id);
//      postRepo.put(id, post);
//      return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
//   }
//   
   @PostMapping(value = "/post")
   public ResponseEntity<Object> createPost(@RequestBody Post post) {
	  String createMsg = postService.create_post(post);
      return new ResponseEntity<>(createMsg, HttpStatus.CREATED);
   }
   

   

}